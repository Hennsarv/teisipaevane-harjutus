﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TeisipäevaneHarjutus
{
    class Program
    {
        static void Main(string[] args)
        {
            // küsib nime
            // küsib vanuse

            // ütleb, mis ta arvab

            Console.Write("Kes sa oled: ");
            string nimi = Console.ReadLine();


            // see variant, kui üks nimi
            //nimi = nimi.ToUpper()[0] + nimi.Substring(1).ToLower();

            // see variant, kui mitu nime (ära küsi kuidas, aga toimib)
            nimi = string.Join(" ", 
                nimi.Split(' ')
                .ToList()
                .Select(x => x.ToUpper()[0] + x.Substring(1).ToLower())
                .ToArray());


            // seda varianti saad kasutada üldisemalt
            //TextInfo textInfo = Thread.CurrentThread.CurrentCulture.TextInfo;
            //nimi = textInfo.ToTitleCase(nimi);


            Console.Write("Vana sa oled: ");
            int vanus = int.Parse(Console.ReadLine());

            Console.WriteLine("Siis sa oled {0} vanusega {1}", nimi, vanus);

            // proovime teistmoodi

            Console.Write("sinu sünniaasta: ");
            int aasta = int.Parse(Console.ReadLine());

            Console.WriteLine($"sina {nimi} oled siis {DateTime.Now.Year - aasta} aasta vanune");

            // kolmas variant

            Console.Write("Millal sa sündisid: ");
            DateTime sünniaeg = DateTime.Parse(Console.ReadLine());

            Console.WriteLine("Sinu vanus {0}", (int)((DateTime.Now - sünniaeg).TotalDays / 365.25));

        }
    }
}
